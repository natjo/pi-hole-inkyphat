from inky import InkyPHAT
from PIL import Image, ImageFont, ImageDraw
from font_fredoka_one import FredokaOne
from inky.auto import auto
from urllib import request
import json
import os
import datetime

FONT_DATE = ImageFont.truetype(FredokaOne, 12)
FONT_MAIN = ImageFont.truetype(FredokaOne, 22)
FONT_PERC = ImageFont.truetype(FredokaOne, 32)

# Relative path to this file
# logo_path = "./logo.png"
logo_path = "./penis.png"


def main():
    # Get values to show
    queries_total, queries_blocked, percent_blocked = get_pihole_data()
    queries_total_str = f'Total: {queries_total:,}'
    queries_blocked_str = f'Blocked: {queries_blocked:,}'
    percent_blocked_str = f'{percent_blocked:.2f} %'

    # Get display
    inky_display = auto(ask_user=True, verbose=False)

    # Get empty base image
    img = Image.new("P", inky_display.resolution)
    draw = ImageDraw.Draw(img)

    # Add time
    time_date = datetime.datetime.now().strftime('%H:%M - %a')
    draw.text((0, 0), time_date, inky_display.BLACK, font=FONT_DATE)

    # Add logo
    logo = Image.open(logo_path)
    logo_w, logo_h = logo.size
    img_w, img_h = img.size
    # Center horizontically and move all the way to the right
    offset = ((img_w - logo_w), int((img_h - logo_h) / 2))
    offset = (offset[0] - 10, offset[1])
    img.paste(logo, offset)

    # Calculate relevant coordinates
    w_t, h_t = FONT_MAIN.getsize(queries_total_str)
    w_b, h_b = FONT_MAIN.getsize(queries_blocked_str)
    w_p, h_p = FONT_PERC.getsize(percent_blocked_str)
    max_text_width = max([w_t, w_b, w_p])
    y = int((inky_display.HEIGHT - (h_t + h_b + h_p)) / 2)
    x = 0
    x_p = int((max_text_width - w_p)/2)

    # Draw text
    draw.text((x, y), queries_total_str, inky_display.BLACK, font=FONT_MAIN)
    y = y+h_t
    draw.text((x, y), queries_blocked_str, inky_display.BLACK, font=FONT_MAIN)
    y = y+h_b
    draw.text((x_p, y), percent_blocked_str, inky_display.BLACK, font=FONT_PERC)
    percent_blocked_y_end = y+h_p

    # Draw progress bar
    blocks_last_10_min = get_pihole_blocks_last_10_min()
    img_w, img_h = img.size
    max_bar_h = inky_display.HEIGHT - percent_blocked_y_end
    max_blocks = max(blocks_last_10_min)
    x_p = 0
    bar_width = 1
    for num_blocks in blocks_last_10_min:
        bar_h = int(max_bar_h * (num_blocks/max_blocks))
        draw.rectangle(((x_p, img_h - bar_h), (x_p+bar_width, img_h)), fill=inky_display.BLACK)
        x_p = x_p + bar_width

    # Show everything
    inky_display.set_image(img)
    inky_display.show()

def get_pihole_data():
    try:
        f = request.urlopen('http://pi.hole/admin/api.php')
        json_string = f.read()
        parsed_json = json.loads(json_string)
        queries_total = parsed_json['dns_queries_today']
        queries_blocked = parsed_json['ads_blocked_today']
        percent_blocked = parsed_json['ads_percentage_today']
    except err:
        print(err)
        queries_total = -1
        queries_blocked = -1
        percent_blocked = -1
    return queries_total, queries_blocked, percent_blocked


def get_pihole_blocks_last_10_min():
    try:
        f = request.urlopen('http://pi.hole/admin/api.php?overTimeData10mins')
        json_string = f.read()
        parsed_json = json.loads(json_string)
        blocks = parsed_json['domains_over_time']
        blocks_last_10_min = list(blocks.values())
    except err:
        print(err)
        blocks_per_min = [0 for _ in range(144)]
    return blocks_last_10_min

if __name__ == '__main__':
    # Change into current directory
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    main()
