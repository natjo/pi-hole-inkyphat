# Pi-hole-inkyphat

Simple Python script to display Pi-hole stats on an inkyphat ink display. Based on https://github.com/neauoire/inky-hole

## Installation
Install inkyphat requirements as described on their website.

## Cronjob
To refresh the display regularly, add this line to your crontab (`crontab -e`):
```
* /30 * * * /usr/bin/python3 /home/pi/pi-hole-inkyphat/main.py
```
Don't forget to adjust the directory the `main.py` script is at
